﻿using BookManagementSamples.Domains;

namespace BookManagementSamples.Applications
{
    public interface IRepository
    {
        IEnumerable<Book> GetAll();
        Book FindById(int id);
        bool Add(Book newBook);
        Book FindByKeyword(string keyword);
        Book DeleteById(int id);
        bool BorrowBook(int id, string borrower);
        bool ReturnBook(int id);
    }
}
