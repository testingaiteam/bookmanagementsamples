﻿using BookManagementSamples.Applications;
using BookManagementSamples.Domains;

namespace Samples.Applications
{
    public class BookService : IBookService
    {
        private readonly IRepository _repository;

        public BookService(IRepository repository)
        {
            _repository = repository;
        }
        public IEnumerable<Book> GetAllBooks()
        {
            return _repository.GetAll();
        }
        public (BookServiceError, Book) GetBook(int id)
        {
            var book = _repository.FindById(id);
            if (book == null)
            {
                return new(BookServiceError.BookIdNotFound, null);
            }
            else
            {
                return new(BookServiceError.Success, book);
            }
        }
        public (BookServiceError, Book) AddBook(Book book)
        {
            throw new NotImplementedException();
        }

        public (BookServiceError, Book) BorrowBook(int id, string borrower)
        {
            throw new NotImplementedException();
        }
        public (BookServiceError, Book) DeleteBook(Book book)
        {
            throw new NotImplementedException();
        }
        public (BookServiceError, Book) ReturnBook(int id, string borrower)
        {
            throw new NotImplementedException();
        }
    }
}
