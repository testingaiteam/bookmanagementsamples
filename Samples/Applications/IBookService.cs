﻿using BookManagementSamples.Applications;
using BookManagementSamples.Domains;
using Microsoft.AspNetCore.Mvc;

namespace Samples.Applications
{
    public interface IBookService
    {
        public IEnumerable<Book> GetAllBooks();
        public (BookServiceError, Book) GetBook(int id);
        public (BookServiceError, Book) AddBook(Book book);
        public (BookServiceError, Book) DeleteBook(Book book);
        public (BookServiceError, Book) BorrowBook(int id, string borrower);
        public (BookServiceError, Book) ReturnBook(int id, string borrower);
    }
    public enum BookServiceError
    {
        Unknown = -1,
        Success = 0,
        BookIdNotFound,
        BookIsBorrowedAlready,
        BookIsNotBorrowed
    }
}
