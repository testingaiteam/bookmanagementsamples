﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace BookManagementSamples.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class CalculatorController : ControllerBase
    {
        [HttpGet]
        public ActionResult<long> Plus(long a, long b)
        {
            return Ok(a + b);
        }
        [HttpGet]
        public ActionResult<float> Divide(float a, float b)
        {
            if (b != 0)
                return Ok(a / b);
            else
                return BadRequest("The denominator cannot be 0.");
        }
    }
}

