﻿using BookManagementAPI;
using BookManagementSamples.Applications;
using BookManagementSamples.Domains;
using BookManagementSamples.Infrastructures;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Samples.Applications;

namespace BookManagementSamples.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class BooksController : ControllerBase
    {
        private readonly IRepository _repository;
        private readonly IBookService _bookService;

        public BooksController(IBookService bookService)
        {
            bookService = _bookService;
            //_repository = repository;
        }

        [HttpGet]
        public ActionResult<IEnumerable<Book>> GetBooks()
        {
            return Ok(_bookService.GetAllBooks());
        }

        [HttpGet("{id}")]
        public ActionResult<Book> GetBook(int id)
        {
            var (error, book) = _bookService.GetBook(id);
            if (error == BookServiceError.Success)
            {
                return Ok(book);
            }
            else
            {
                return NotFound();
            }
        }

        [HttpPost]
        public ActionResult AddBook(Book book)
        {
            _repository.Add(book);
            return CreatedAtAction("GetBook", new { id = book.Id }, book);
        }

        [HttpPut("{id}")]
        public ActionResult BorrowBook(int id, string borrower)
        {
            var result = _repository.BorrowBook(id, borrower);
            if (result == false)
            {
                return NotFound();
            }
            return Ok();
        }

        [HttpPut("{id}/return")]
        public async Task<IActionResult> ReturnBook(int id)
        {
            var result = _repository.ReturnBook(id);
            if (result == false)
            {
                return NotFound();
            }
            return Ok();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteBook(int id)
        {
            var book = _repository.FindById(id);
            if (book == null)
            {
                return NotFound();
            }
            _repository.DeleteById(book.Id);
            return Ok();
        }
    }
}
