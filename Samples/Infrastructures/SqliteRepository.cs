﻿using BookManagementAPI;
using BookManagementSamples.Applications;
using BookManagementSamples.Domains;
using Microsoft.EntityFrameworkCore;

namespace BookManagementSamples.Infrastructures
{
    public class SqliteRepository : DbContext, IRepository
    {
        private DbSet<Book> Books { get; set; }
        public SqliteRepository(DbContextOptions<SqliteRepository> options) : base(options) { }

        public IEnumerable<Book> GetAll()
        {
            return Books.ToList();
        }

        public Book FindById(int id)
        {
            return Books.Find(id);
        }

        public bool Add(Book newBook)
        {
            Books.Add(newBook);
            return true;
        }

        public Book FindByKeyword(string keyword)
        {
            return null;
        }

        public Book DeleteById(int id)
        {
            var book = FindById(id);
            if (book == null)
            {
                return null;
            }
            else
            {
                Books.Remove(book);
                return book;
            }
        }

        public bool BorrowBook(int id, string borrower)
        {
            var book = FindById(id);
            if (book == null)
            {
                return false;
            }
            else
            {
                book.Borrower = borrower;
                return true;
            }
        }

        public bool ReturnBook(int id)
        {
            var book = FindById(id);
            if (book == null)
            {
                return false;
            }
            else
            {
                book.Borrower = null;
                return true;
            }
        }
    }
}
