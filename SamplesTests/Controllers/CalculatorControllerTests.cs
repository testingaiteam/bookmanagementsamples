﻿using BookManagementSamples.Controllers;
using Microsoft.AspNetCore.Mvc;
namespace BookManagementSamplesTests.Controllers
{
	public class CalculatorControllerTests
	{
        private CalculatorController _controller;

        public CalculatorControllerTests()
		{
            _controller = new CalculatorController();
        }

        [Fact]
        public void Plus_ReturnOk()
        {
            // Arrange
            long a = 10, b = 20;

            // Act
            var result = _controller.Plus(a, b);

            // Assert
            var okResult = Assert.IsType<OkObjectResult>(result.Result);
            Assert.Equal(okResult.Value, (long)30);
        }

        [Theory]
        [InlineData(9, 3, 3)]
        [InlineData(0, 3, 0)]
        [InlineData(-999, -3, 333)]
        [InlineData(121851072, 987, 123456)]
        public void Divide_ReturnOk(float a, float b, float expectedResult)
        {
            // Arrange

            // Act
            var result = _controller.Divide(a, b);

            // Assert
            var okResult = Assert.IsType<OkObjectResult>(result.Result);
            Assert.Equal(okResult.Value, expectedResult);
        }
        [Fact]
        public void Divide_ReturnBadRequest()
        {
            // Arrange
            float a = 1, b = 0;

            // Act
            var result = _controller.Divide(a, b);

            // Assert
            Assert.IsType<BadRequestObjectResult>(result.Result);
        }
    }
}

